&ZERO_body
         Total_Energy = .T.      !< Calculation and output of kinetic, potential, interaction, and total energy.
         Orbitals_Output=.T.     !< Create *orbs.dat ASCII files.
         FTOrbitals_Output=.F.   !< Create *ftorbs.dat ASCII files.
         Coefficients_Output=.T. !< Create *coef.dat ASCII files.
         MPIFFT_binary = .F.     !< If set to true, binary data structure is as from computations with domain decomposed orbitals 
         Time_From=0.0d0         !< Start Analysis at this time.
         Time_To=309.0d0           !< Stop Analysis at this time.
         Time_Points=310           !< Number of points. Step is (T_To - T_From)/T_points.
         MatrixElements_Output=.F.   !< Toggle output of one-body and two-body reduced density matrix elements
         HamiltonianElements_Output=.F. !<  Toggle output of one-body and two-body Hamiltonian matrix elements
         GetA = .F.              !< Toggle separate output of Apair=W_kkjj with k\=j and Adiag=W_jjjj.
         Dilation=1              !< Factor to dilate real space in order to obtain better k-space resolution (Only touched if AutoDilation is false).
         AutoDilation = .F.      !< Toggle if threshold Kdip is used to evaluate optimal dilation for FFTs
         Kdip = 0.0001           !< Threshold to compute optimal dilation for FFTs if AutoDilation is true
/
&ONE_body
         Density_x=.T.   !< Output of the diagonal of the one-body density.
         Density_k=.F.   !< Output of the diagonal of the one-body density in k-space.
!####################  
         Pnot=.F.         !< This calculates the integral of the one-body-density from 
         xstart  = -10.0d0   !< xstart to
         xend	 = 0.d0   !< xend
         ystart = 0.d0    !< Start of P_not integration.
         yend   = 0.d0    !< Stop of P_not integration.        
         zstart = 0.d0    !< Start of P_not integration.
         zend   = 0.d0    !< Stop of P_not integration.
!####################  
         Phase   = .F.    !< Toggle output of orbital and averaged Phase.
         Gradient= .F.    !< Toggle output of the gradient of the Phases
         CavityOrder = .F. !< Toggle output of order parameters Theta and B for cavity treatment (compare Nature 464, 1301 (2010)).
!####################
         CavityLatticeOrder = .F. !< Toggle the output of order parameter imbalance (Theta) for cavity with optical lattice treatment.
         k_op = -1.d0     !< Wavelength in the imbalance operator, -1.d0 means the same as the pump wavelength, -2.d0 means the same as the lattice wavelength (parameter3).
         phi_op=0.d0      !< Phase shift in the imbalance operator, -2.d0 means the same as the lattice phase shift (parameter4).
         Variance=.F.     !< Toggle the output of variance <N^2>-<N>^2.
/
&TWO_body
         Correlations_X=.F.   !< Output of reduced one-body and two-body densities.
         Correlations_K=.F.   !< Output of reduced one-body and two-body densities in k-space.
!####################  
         StructureFactor=.F.  !< Output of dynamic structure factor and local correlation function
         xref=0.d0            !< x value of reference point of dynamic structure factor
         yref=0.d0            !< y value of reference point of dynamic structure factor
         zref=0.d0            !< z value of reference point of dynamic structure factor
!####################  
         Correlation_Coefficient=.F. !< outpus of the correlation coefficient tau.
!####################  
         Geminals=.F.         !< Toggle output of natural geminal occupations in GO_PR.out
         FullGeminals=.F.     !< Toggle output of natural geminals in <time>NzMy-x-geminals.dat files
!####################  
         corr1restr=.F   !< Output of reduced one-body density in a 1D subspace (for big grids).
         xini1=-4.0d0    !< from x
         xfin1=4.d0      !< to x
         xpts1=9999      !< desired number of points
!####################  
         corr1restrmom=.F.   !< Output of reduced one-body density in a 1D subspace (for big grids) in k-space.
         kxini1=-4.d0        !< from k 
         kxfin1=4.d0         !< to k
         kpts1=2048          !< desired number of points
!####################  
         corr2restr=.F.       !< activate rho^2(x_1,x_2)
         xini2=-4.0d0         !< from x
         xfin2=4.d0           !< to x
         xpts2=2048           !< desired no of pts
!####################  
         corr2restrmom=.F.    !< activate rho^(2)(k_1,k_2)
         kxini2=-4.d0         !< from k 
         kxfin2=4.d0          !< to k
         kpts2=2048           !< desired no of pts
/
&MANY_BODY
         lossops=.F.         !< compute the different integrals 
         border=0.d0         !< on the partitioned (at 'border') N=2 Hilbertspace
!####################  
         NO_Coeffs = .F.     !< Calculate coefficients in the natural orbital basis (M=2 only)
!####################  
         Entropy = .F.    !< Toggle the output of various entropy measures.
         NBody_C_Entropy = .F.    !< Toggle the output of n-body coefficient's entropy.
         TwoBody_Entropy = .F.    !< Toggle output of Shannon entropy of two-body density matrix (in real & momentum space)
!####################  
         SingleShot_Analysis = .T. !< Toggle the output of random deviates of the N-body density matrix.
         SingleShot_FTAnalysis = .F. !< Toggle the output of random deviates of the N-body momentum density matrix.
         SingleShot_Convolution = .F. !< Convolute single shots with point-spread function?
         Convolution_Width = 3 !< Width of point spread function to convolute the single shots with in gridpoints.
         NShots = 3000             !< Number of single shots (random deviates of the N-body density) to compute.
         CentreOfMass = .F.      !< Toggle sampling of centre-of-mass operator
         CentreOfMomentum = .F.  !< Toggle sampling of centre-of-momentum operator
         NSamples_COM = 100         !< Number of samples to make from the centre-of-mass operator  
         ShotVariance=.F.        !< Compute integrated variance of single shots.
         NSamples_VAR = 100    !< Number of samples to take to compute the variance from 
         Binning=.F.                    !< Compute Even-Odd imbalance histogram from a set of single-shots 
         Binning_Period=3.1415926536d0  !< Periodicity of Even-Odd imbalance sampling
         Binning_Samples=10000          !< Number of samples to use for the Even-Odd imbalance sampling
!####################  
         anyordercorrelations_X = .F. !< toggle output of higher order correlations rho(ref_r,...,ref_r,r_order-1,r_order-2)  
         anyordercorrelations_K = .F. !< toggle output of higher order momentum correlations rho(ref_r,...,ref_r,r_order-1,r_order-2)
         order = 10 !< order up to which correlation functions are output
         oneD = .T. !< toggle computation of higher order correlation functions as a function of a single variable     
         twoD = .F. !< toggle computation of higher order correlation functions as a function of two variables
         c_ref_x = 0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0 !< x-value of reference points of correlation functions 
         c_ref_y = 0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0 !< y-value of reference points of correlation functions 
         c_ref_z = 0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0 !< z-value of reference points of correlation functions
!#####################         
         fidelity   = .T. !< Calculate autocorrelation or fidelity function
         single_ref = .TRUE. !< False: fidelity, i.e., |<Psi1(t)|Psi2(t)>|**2, TRUE: autocorrelation |<Psi(0)|Psi(t)>|**2
         t_ref      = 0.d0 !< Reference time for the autocorrelation function.
         CI_Reference = 'CI0' !< filename for the coefficients of the reference wavefunction for autocorrelation
         PSI_Reference = 'PSI0' !< filename for the orbitals of the reference wavefunction for autocorrelation
         Header_Reference = 'Head0' !< filename for the header of the reference wavefunction for autocorrelation
!#####################
         FDF_X = .FALSE. !< Compute histogram of counts in position space from a number of single shots given by FDF_Samples 
         FDF_K = .FALSE. !< Compute histogram of counts in momentum space from a number of single shots given by FDF_Samples
/
&TWO_D         
!####################  this gives the correlation functions for 2D problems in cuts
          MOMSPACE2D=.F.       !< activate realspace or momentum space output of 
          REALSPACE2D=.F.      !< rho^2(x_1x,x_1y,x_2x,x_2y) and
          REALSKEW2D = .F.     !< Toggle output of 2D skew diagonal correlation function.
          MOMSKEW2D  = .F.     !< Toggle output of 2D skew diagonal correlation function in k-space.
          x1const=.T.          !< rho^1(x_1x,x_1y,x'_1x,x'_1y).  
          x1slice=3.d0         !< Two coordinates of four have to be kept constant to obtain
          y1const=.T.          !< a 3-D plot. Control variables are 
          y1slice=3.d0         !< x1const,x2const,y1const(x'_1x),y2const(x'_1y)
          x2const=.F.          !< Their values are set by the variables with the 'slice' suffix.
          x2slice=0.d0
          y2const=.F.
          y2slice=0.d0
          PROJ_X=.F.           !< calculate the projection 
                               !< V(X or Y)=sum_ij rho_ij <phi_i|v_(x,y)|phi_j>_X/Y/<phi_i|phi_j>_X/Y
          DIR='X'              !< if ='X' V(X), if = 'Y' V(Y), if = 'B' V(X) and V(Y) are given
          L_Z=.F.              !< give orbital and total angular momentum for 2D FFT calculations
/

