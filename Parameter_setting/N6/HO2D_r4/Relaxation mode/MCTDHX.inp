&System_Parameters
!c==========
!c========== System definition
!c==========
!JOB_TYPE = 'BOS'                 !c========== Standard MCTDHB 
JOB_TYPE = 'FER'                !c========== Standard MCTDHF 
!JOB_TYPE = 'FCI'                !c========== Full CI 

Morb = 7                       !c========== Number of Orbitals
Npar = 6                       !c========== Number of Particles 
xlambda_0 = +1.0e0               !c========== Strength of the interparticle interaction 
mass = 1.0d0
Job_Prefactor = (-1.0d0,0.0d0)   !c========== (0.0d0,-1.0d0) Forward propagation
                               !c========== (0.0d0,+1.0d0) Backward propagation
                               !c========== (-1.0d0,0.0d0) "Improved" Relaxation 
!GUESS = 'BINR'                  !c========== Restart from binary files
!GUESS = 'DATA'                  !c========== Restart from ASCII files
GUESS = 'HAND'                   !c========== Start from the initial state specified in the Get_Initial_Orbitals and Get_Initial_Coefficients routines.

NProjections = 2                 !c========== How often to apply the projection operator
Diagonalize_OneBodyh = .F.       !c========== Start from the eigenfunctions of the one-body Hamiltonian h as orbitals.
Binary_Start_Time = 0.0d0        !c========== If GUESS='BINR' start from this point in time.
Restart_State = 1              !c========== State from which to restart from a previous block relaxation.
Restart_Orbital_FileName = '10.0000000orbs.dat'      ! If GUESS='DATA' then the orbitals are read from this ASCII file.
Restart_Coefficients_FileName = '10.0000000coef.dat' ! If GUESS='DATA' then the coefficients are read from this ASCII file.
Custom_Orbital_Initialization = .F. !c========== If GUESS='HAND' Do we use custom initial orbital?
Which_Custom_Orbital_Initialization = 'Random' !c========== If GUESS='HAND' Which custom initial orbital do we use?
Custom_Orbital_Initial_Parameters = 0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0  !c========== Parameters for specifying the initial orbital.

Fixed_LZ = .F.                   !c========== In relaxations: Apply a projection to a fixed orbital phase profile? 
OrbLz = 0,0,0,0,0,0,0,0,0,0      !c========== How many times 2\pi the phase is going to jump. If any value is -666, the respective orbital is unchanged.
Vortex_Seeding = .F.             !c========== In propagations: Multiply initial orbitals with phase/density profile?
Vortex_Imprint = .F.             !c========== In relaxations: Apply a projection operator to a certain orbital density profile?
Profile = 'tanh'                 !c========== If Vortex_Imprint is true, this will select the respective shape of the imprinted profile.
NonInteracting = .F.             !c========== Omit two-body part of the Hamiltonian?
!c==========
!c========== Multilevel treatment
!c==========
Multi_level = .F.              !c========== Treat problem with multi-level orbitals
NLevel = 1                     !c==========  Number of levels if Multi_level computation
InterLevel_InterParticle = .F. !c========== Interlevel interparticle interactions in a multilevel computation
Spinor = .F.                   !c========== Interlevel interparticle interaction is realized as a spin algebra? 
SpinOrbit = .F.                !c========== Toggles inclusion of spin-orbit-interaction in the Hamiltonian 
Rashba_Prefactor=0.d0          !c========== Magnitude of Rashba-spin-orbit-interaction
Dresselhaus_Prefactor=0.d0     !c========== Magnitude of Dresselhaus-spin-orbit-interaction
SpinOrbit_Prefactor=0.d0       !c========== Magnitude of total spin-orbit (== Rashba + Dresselhaus) term.
Conical_Intersection = .F.     !c========== If the different internal levels of the considered atoms are coupled through a conical intersection.
xlambda1=0.d0                  !c========== Intra-level interaction strength for multilevel computations.
xlambda2=0.d0                  !c========== Intra-level interaction strength for multilevel computations.
xlambda12=0.d0                 !c========== Inter-level interaction strength for multilevel computations.
xlambda3=0.d0                  !c========== Intra-level interaction strength for multilevel computations.  
Lambda1=0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0 !c========== array of interaction strengths used for the spin-independent part of the interparticle interaction for spinors 
Lambda2=0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0 !c========== array of interaction strengths used for the spin-dependent part of the interparticle interaction for spinors 
!c==========
!c========== Artificial gauge fields
!c==========
SyntheticMagnetic=.F.          !c========== Toggle for including an a synthetic magnetic field in z-direction (2D problems)
charge=1.d0                    !c========== value for the artificial charge of the particles
magnetic_strength=1.d0         !c========== strength of the artificial magnetic field
Magnetic_field_type=1          !c========== 
a_vec=0.d0                     !c========== 
b_vec=0.d0                     !c========== 
!c==========
!c========== Bose-Hubbard/Lattice treatment
!c==========
Bose_Hubbard=.F.               !c========== Do configuration interaction with a Bose-Hubbard Hamiltonian.
Calculate_Rho2 =.T.            !c========== Toggle calculation of the two-body density in Bose-Hubbard.
Periodic_BH=.F.                !c========== Toggle periodic boundary conditions for BH Hamiltonians.
BH_J=(0.d0,0.d0)               !c========== BH Nearest neighbor coupling J.
BH_U=0.d0                      !c========== BH onsite interaction U.
!c==========
!c========== BEC--Cavity coupled treatment
!c==========
Cavity_BEC = .F.               !c========== toggle to couple the MCTDH-X equations to a cavity field
NCavity_Modes = 1              !c========== Number of cavity modes
Cavity_PumpRate =20.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0       !c========== Rate at which the cavity is pumped
Cavity_LossRate =11.2d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0       !c========== Rate at which it loses photons
Cavity_Detuning =43.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0       !c========== Cavity detuning
Cavity_K0 =4.9d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0             !c========== Cavity resonance 
CavityAtom_Coupling =0.46d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0   !c========== Coupling strength of the atoms to the cavity
X_Cavity_Pump_Waist =0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0   !c========== For 2D systems: X- Waist of pump beam.
Cavity_Mode_Waist =0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0     !c========== For 2D systems: Waist of cavity beam.
Pump_Switch = .F.,.F.,.F.,.F.,.F.,.F.,.F.,.F.,.F.,.F.                    !c========== Smoothly switch on cavity pump laser.
RampupTime =0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0            !c========== Rampup time for exponential ramp.
RampdownTime =0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0          !c========== Rampdown time for exponential ramp.
PlateauTime =0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0           !c========== Plateau time in pump power.
Pump_Oscillate = .F.,.F.,.F.,.F.,.F.,.F.,.F.,.F.,.F.,.F.                 !c========== Pump power is oscillating when the plateau is reached
Pump_Amplitude =0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0        !c========== Amplitude of the oscillation of the pump's power as a fraction of Cavity_PumpRate
Pump_Period =0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0           !c========== At what period the power is oscillating.
Custom_Cavity=.F.                                                        !c========== Use "Get_Cavity_Modes.f90" routine to specify cavity and pump modes
Which_Cavity='SinCos'                                                    !c========== Specify which cavity-profiles are used if Custom_Cavity=.T. 
Transverse_Pump = .F.,.F.,.F.,.F.,.F.,.F.,.F.,.F.,.F.,.F.                !c========== In a one-dimensional setup: is the pump transversal or not?
Detuning_Switch = .F.
Detuning_Switch_Mode = 'linear'
Detuning_SwitchTime = 0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0
Detuning_SwitchValue = 0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0
!c==========
!c========== CRAB-Optimal Control Theory run  
!c==========
DO_OCT =.F.                    !c========== Perform CRAB-optimal control
OCT_Alpha=1.d0                 !c========== Weight of first part of Crab functional.
OCT_Beta=0.2d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0,0.d0 !c Weight of functional penalty for each goal.
OCT_NGoals=1                   !c========== How many goals to pursue in optimization.
OCT_NC=5                       !c========== How many frequency components to take for the optimization.
OCT_TimeSteps=10000            !c========== How many points are used to represent the time-evolution of the controls Gamma_k(t)
OCT_Func_Tolerance=0.0001      !c========== Error threshold for the convergence of the Optimal Control Functional
OCT_CRAB_Tolerance=0.0001      !c========== Error threshold for the convergence of the Controls
/
&DVR
DIM_MCTDH=2                       ! Dimensionality of the Problem
NDVR_X = 128                        ! Number of DVR points in X-direction. In the case of BH-Hamiltonians, the number of sites in the X direction.
NDVR_Y = 128                          ! Number of DVR points in Y-direction. In the case of BH-Hamiltonians, the number of sites in the Y direction.
NDVR_Z = 1                          ! Number of DVR points in Z-direction. In the case of BH-Hamiltonians, the number of sites in the Z direction.

DVR_X = 4                           ! DVR Methods:      1      ||  3       ||   4             ||    5
                                  !        Harmonic Osc. DVR || Sine DVR || FFT Collocation || ExpDVR
DVR_Y = 4                           ! For MCTDBH select DVR_X=6
                                  ! For MCTDBH select DVR_Y=6
DVR_Z = 4                           ! For MCTDBH select DVR_Z=6
                                  
x_initial = -8.0d0                  !< Spatial extension of the grid, X-direction 
x_final = +8.0d0                    !< Spatial extension of the grid, X-direction 
y_initial = -8.0d0                  !< Spatial extension of the grid, Y-direction 
y_final = +8.0d0                    !< Spatial extension of the grid, Y-direction 
z_initial = -8.0d0                 !< Spatial extension of the grid, Z-direction 
z_final = +8.0d0                   !< Spatial extension of the grid, Z-direction 
/

&Integration
Time_Begin = 0.0d0                  !< Initial Time point
Time_Final = 30.0d0                 !< Final Time point
Time_Max = 1.d99                    !< Overall Maximal time
Output_TimeStep = 1.d0             !< When to write data. 
Output_Coefficients = 1             !< How often to write the Coefficients in units of Output_TimeStep
Integration_Stepsize = 0.1d0        !< Stepsize of the integration (for propagation use 0.01d0) 
Error_Tolerance = 1.0d-9            !< Error tolerance of Integration
Minimal_Occupation = 1.0d-12        !< Threshold to set occupations to 0d0 in order to recover the invertibility of the Reduced one-body density.
Minimal_Krylov=5                  !< Minimal Krylov subspace
Maximal_Krylov=40                 !< Maximal Krylov subspace
Orbital_Integrator='RK'           !< Integrator for the orbital equations (BS,ABM,OMPABM,MPIABM,RK)
MPI_ORBS=.F.                      !< Trigger domain decomposed orbitals (need to use this in connection with MPIABM or MPIRK for the Orbital_Integrator)
NProc_Max_CI=1                    !< If the MPI_ORBS is .T. , the number of processes for the CI part can be restricted using this input. 
Orbital_Integrator_Order=8        !< order of the orbital integrator (Max. 7 for ABM/OMPABM, Max. 16 for BS, 5 or 8 for RK)
Orbital_Integrator_MaximalStep=0.01d0  !< maximal step for orbital equations integration 
Write_ASCII=.T.                   !< Toggle ASCII output.
Error_Rescale_TD=1.0d0            !< Scaling of the error in Time-Dependent external potentials.
Error_Rescale_Cavity=1.0d0       !< Scaling of the error for the integration of the cavity equation of motion
LZ=.F.                            !< (de)activate angular momentum operator in z direction
OMEGAZ=0.0d0                      !< set the angular frequency
STATE=1                           !< select which eigenstate is computed (STATE=1 is the groundstate)
Coefficients_Integrator='DAV'     !< Integrator for Coefficients equations (DAV,BDV,MCS,DSL)
BlockSize = 4                     !< If Block Davidson (BDV) is used then this selects the number of coefficient vectors in the block.
Olsen=.F.                         !< If Block Davidson is used, this flag toggles the Olsen correction (== Jacobi-Davidson)
RLX_Emin = -1.d90                 !< If Block Davidson is used then this selects the lower energy bound for the block.
RLX_Emax = 1.d90                  !< If Block Davidson is used then this selects the upper energy bound for the block.
/
&Potential
whichpot="HO2D*sin"                   !< Potential Name - Character of length 8 
parameter1=1.d0                   !< Potential Parameter 1 Real 
parameter2=0.d0                   !< Potential Parameter 2 Real 
parameter3=2.d0                   !< Potential Parameter 3 Real 
parameter4=1.d0                   !< Potential Parameter 4 Real 
parameter5=0.001d0                   !< Potential Parameter 5 Real 
parameter6=0.d0                   !< Potential Parameter 6 Real 
parameter7=0.d0                   !< Potential Parameter 7 Real  
parameter8=0.d0                   !< Potential Parameter 8 Real 
parameter9=0.d0                   !< Potential Parameter 9 Real 
parameter10=0.d0                  !< Potential Parameter 10 Real 
parameter11=0.d0                  !< Potential Parameter 11 Real 
parameter12=0.d0                  !< Potential Parameter 12 Real 
parameter13=0.d0                  !< Potential Parameter 13 Real 
parameter14=0.d0                  !< Potential Parameter 14 Real 
parameter15=0.d0                  !< Potential Parameter 15 Real 
parameter16=0.d0                  !< Potential Parameter 16 Real 
parameter17=0.d0                  !< Potential Parameter 17 Real 
parameter18=0.d0                  !< Potential Parameter 18 Real 
parameter19=0.d0                  !< Potential Parameter 19 Real 
parameter20=0.d0                  !< Potential Parameter 20 Real 
parameter21=0.d0                  !< Potential Parameter 21 Real 
parameter22=0.d0                  !< Potential Parameter 22 Real 
parameter23=0.d0                  !< Potential Parameter 23 Real 
parameter24=0.d0                  !< Potential Parameter 24 Real 
parameter25=0.d0                  !< Potential Parameter 25 Real
parameter26=0.d0                  !< Potential Parameter 26 Real
parameter27=0.d0                  !< Potential Parameter 27 Real
parameter28=0.d0                  !< Potential Parameter 28 Real
parameter29=0.d0                  !< Potential Parameter 29 Real
parameter30=0.d0                  !< Potential Parameter 30 Real
/
&Interaction
Interaction_Type=0                !< Wxx_TYPE=0:  W(r,r')=Delta(x-x')
                                  !< Wxx_TYPE=1:  W(r,r')=W(x-x')*W(y-y')*W(z-z')
                                  !< Wxx_TYPE=2:  W(r,r')=W(r-r') Tridiagonal representation for aequidistant grids (DVRs 3,4 and 5)
                                  !< Wxx_TYPE=3:  W(r,r')=W(r,r') Use the full Interaction Matrix: W(i,j) Dim(W)=(NDX*NDY*NDZ * NDX*NDY*NDZ)
                                  !< Wxx_TYPE=4:  W(r,r')= FFT^(-1)[W(k-k')*f_sl(k-k')] Interaction Matrix Evaluation by succesive FFTs
                                  !< Wxx_TYPE=5:  W(r,r';t)= FFT^(-1)[W(k-k')*f_sl(k-k',t)] Interaction Matrix Evaluation by succesive FFTs
                                  !< Wxx_TYPE=6:  W(r,r';t)= lambda(t) Delta(x-x') 
                                  !< Wxx_TYPE=7:  W(r,r';t)= lambda Delta(x-x') + FFT^(-1)[W(k-k')*f_sl(k-k')] Contact and Non-zero ranged interaction.
which_interaction='gauss'         !< select predefined interparticle interaction
Interaction_Width=0.15d0          !< In case of finite range interaction potential (Interaction Type 4 ONLY!) set interaction width.  
Interaction_Parameter1=0.5d0      !< Parameters to modify the interparticle interaction
Interaction_Parameter2=0.0d0      !< Parameters to modify the interparticle interaction
Interaction_Parameter3=0.0d0      !< Parameters to modify the interparticle interaction
Interaction_Parameter4=0.0d0      !< Parameters to modify the interparticle interaction
Interaction_Parameter5=0.0d0      !< Parameters to modify the interparticle interaction
Interaction_Parameter6=0.0d0      !< Parameters to modify the interparticle interaction
Interaction_Parameter7=0.0d0      !< Parameters to modify the interparticle interaction
Interaction_Parameter8=0.0d0      !< Parameters to modify the interparticle interaction
Interaction_Parameter9=0.0d0      !< Parameters to modify the interparticle interaction
Interaction_Parameter10=0.0d0     !< Parameters to modify the interparticle interaction
/
