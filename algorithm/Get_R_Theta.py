import numpy as np
import sys
#define the global constant pi=3.1415926....
global PI,div_r,div_t,bound
PI=np.pi
div_r=32;
div_t=36;
bound=8;



def read_data(path):
	data=np.loadtxt(path);
	dim=np.shape(data);
	return data,dim

def polar(x,y):
	r=np.sqrt(x**2+y**2);
	PI=np.pi;
	if (x==0 and y>0):
		theta=PI/2;
	elif (x==0 and y<0):
		theta=PI*3/2;
	elif (x<0):
		theta=PI+np.arctan(y/x);
	elif (x>0 and y<0):
		theta=2*PI+np.arctan(y/x)
	else:
		theta=np.arctan(y/x);
	return r,theta

def get_r_theta(img,dim,div_r,div_t):
	r_step=min(dim)/div_r;
	theta_step=2*PI/div_t;
	Ra=np.zeros(div_r);
	The=np.zeros(div_t);
	rx=0.5;
	ry=0.5;
	if (dim[0]%2!=0):
		rx=0;
	if (dim[1]%2!=0):
		ry=0;
	for i in range(0,dim[0]-1):
		for j in range(0,dim[1]-1):
			x=i-dim[0]/2+rx;
			y=j-dim[1]/2+ry;
			r,theta=polar(x,y);
			if r>=min(dim):
				continue
			else:
				r_index=int(r//r_step);
				t_index=int(theta//theta_step);
				Ra[r_index]=Ra[r_index]+img[i][j];
				The[t_index]=The[t_index]+img[i][j];
	return Ra/sum(Ra),The/sum(The)

def get_var(data):
	return np.var(data);

def get_meam_r(R,data):
	mean_r=np.sum(R*data)
	return mean_r


# path=sys.argv[1];
# save_path=sys.argv[2];
path="/home/jiabing/ResearchTraining/Master_Thesis/data/Gau/N6/10/density";
save_path="/home/jiabing/ResearchTraining/Master_Thesis/data/Gau/N6/10/test2.txt";
var=[];
mean_r=[];
ran=range(0,310);
for i in ran:
	print("Image ",i)
	img,dim=read_data(path+'/'+str(i)+'.txt');
	bound=min(dim)/128*8;
	R=np.linspace(0,bound,div_r);
	T=np.linspace(0,360,div_t);
	radius,theta=get_r_theta(img,dim,div_r,div_t);
	var.append(get_var(theta));
	mean_r.append(get_meam_r(R,radius));
np.savetxt(save_path,(ran,var,mean_r),delimiter=' ',fmt="%.8f");