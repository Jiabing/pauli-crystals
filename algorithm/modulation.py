import numpy as np
import matplotlib.pyplot as plt

def freq(w0,A,t):
	return w0+A*np.sin(2*w0*t)

toffset=-0.01;
time=np.linspace(0,0.05,10000,endpoint='T');
w0=2*np.pi*983;
A=0.1;
plt.figure(figsize=(15,7));
plt.plot([toffset,0],[w0,w0],linestyle='dashed',linewidth=5,label='Fermions in the ground state');
plt.plot(time,freq(w0,A,time),color='red',label='Shaking amplitue A=10%');
plt.plot([max(time),max(time)+0.02],[w0,w0],color='g',linewidth=3,label='Waiting period');
plt.plot([max(time)+0.02,max(time)+0.02],[w0,0],color='g',linewidth=3)
plt.ylim(w0*0.99995,w0*1.00005);
plt.xlim(toffset,max(time)+0.05);
plt.xticks([0,0.05],labels=['0',r'$t_s$'],fontsize=20);
plt.tick_params(axis='x',direction='in');
plt.yticks([w0*0.99995,w0],labels=[0,r'$\omega_0$'],fontsize=20);
plt.ylabel(r'Radial trap frequency $\omega_r(t)$',fontsize=20);
plt.xlabel('Time',fontsize=20);
plt.legend(fontsize=20)
plt.text(max(time)+0.02,w0*0.999945,r"$t_{TOF}$",fontsize=20)
#plt.title(r"Trap frequency modulation $\omega_r(t)$ Eq. 2.2.1",fontsize=20)
plt.show()
