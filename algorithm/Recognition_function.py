import numpy as np
import matplotlib.pyplot as plt

#define the global constant pi=3.1415926....
global PI
PI=np.pi

def read_data(path):
	data=np.loadtxt(path);
	dim=np.shape(data);
	return data,dim

def polar(x,y):
	r=np.sqrt(x**2+y**2);
	PI=np.pi;
	if (x==0 and y>0):
		theta=PI/2;
	elif (x==0 and y<0):
		theta=PI*3/2;
	elif (x<0):
		theta=PI+np.arctan(y/x);
	elif (x>0 and y<0):
		theta=2*PI+np.arctan(y/x)
	else:
		theta=np.arctan(y/x);
	return r,theta

def get_r_theta(img,dim,div_r,div_t):
	r_step=min(dim)/div_r;
	theta_step=2*PI/div_t;
	Ra=np.zeros(div_r);
	The=np.zeros(div_t);
	rx=0.5;
	ry=0.5;
	if (dim[0]%2!=0):
		rx=0;
	if (dim[1]%2!=0):
		ry=0;
	for i in range(0,dim[0]-1):
		for j in range(0,dim[1]-1):
			x=i-dim[0]/2+rx;
			y=j-dim[1]/2+ry;
			r,theta=polar(x,y);
			if r>=min(dim):
				continue
			else:
				r_index=int(r//r_step);
				t_index=int(theta//theta_step);
				Ra[r_index]=Ra[r_index]+img[i][j];
				The[t_index]=The[t_index]+img[i][j];
	return Ra,The

def variance(data):
	return np.var(data)

which='B6'
path='/home/jiabing/ResearchTraining/Master_Thesis/data/density/'+which+'.txt';
which1='B6'
path1='/home/jiabing/ResearchTraining/Master_Thesis/data/singleshots/'+which1+'.txt';
div_r=32;
div_t=36;
bound=8;
img,dim=read_data(path);
bound=min(dim)/128*8;
R=np.linspace(0,bound,div_r);
T=np.arange(0,360,360/div_t);
radius,theta=get_r_theta(img,dim,div_r,div_t);
img1,dim1=read_data(path1);
radius1,theta1=get_r_theta(img1,dim1,div_r,div_t);
DV=variance(theta/sum(theta));
CV=variance(theta1/sum(theta1));
print('B6 density:%.8f' % DV);
print('B6 singleshots:%.8f' % CV);
print(CV/DV)
fig = plt.figure(figsize=(10,8))
T_plot=np.arange(0,361,360/div_t);
the_plot=theta/sum(theta);
the_plot1=theta1/sum(theta1);
the_plot=np.append(the_plot,the_plot[0]);
the_plot1=np.append(the_plot1,the_plot1[0]);
plt.plot(T_plot,the_plot,linestyle='-',marker='s',color='blue',linewidth=2,markersize=6,label=r'One-body density: $\rho_d(\theta)$');
plt.plot(T_plot,the_plot1,linestyle='-',marker='o',color='red',linewidth=2,markersize=6,label=r'Configuration density: $\rho_s(\theta)$');
plt.legend(fontsize=20);
plt.yticks(fontsize=20)
plt.xticks(np.linspace(0,360,7,endpoint='T'),fontsize=20);
plt.xlabel(r"$\theta$/degree",fontsize=25);
plt.ylabel("Density",fontsize=25)
plt.xlim(0,360);
plt.ylim(0,0.06);
plt.title("(b) Six bosons",fontsize=25)
plt.show()
plt.close()
plt.show()