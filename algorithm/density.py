import numpy as np
import sys
import matplotlib.pyplot as plt

def input_data(path,dimx,dimy):
	c=3;
	data=np.loadtxt(path);
	Z=np.reshape(data[:,c],(dimy,dimx));
	X=data[0:dimx,0];
	Y=np.reshape(data[:,1],(dimy,dimx))[:,0];
	return Z,X,Y


def plot_image(img,X,Y,bound):
	fig = plt.figure(figsize=(10,8))
	cax=plt.pcolor(X,Y,img,cmap='rainbow',shading='auto');
	cbar = plt.colorbar(cax, drawedges = False)
	cbar.set_label(r'Density $\rho(x,y)$',size=25, weight =  'bold')
	cbar.ax.tick_params( labelsize=25 )
	cbar.minorticks_on()
	plt.xlim(-1*bound/2,bound/2);
	plt.ylim(-1*bound/2,bound/2);
	plt.xticks(fontsize=25);
	plt.yticks(fontsize=25);
	plt.xlabel("x",fontsize=30);
	#plt.ylabel("y",fontsize=30);
	plt.title("(d) Six bosons",fontsize=30)
	return fig


name="0"
path="/home/jiabing/ResearchTraining/Master_Thesis/data/density.dat";
save_path="/home/jiabing/ResearchTraining/Master_Thesis/";
dim=128;
N=3;
dimx=dim;
dimy=dim;
bound=dim/128*8;
X=np.linspace(-1*bound,bound,dimx);
Y=np.linspace(-1*bound,bound,dimy);
img,X,Y=input_data(path,dimx,dimy);
img=img/np.sum(img)*N
np.savetxt(save_path,img,fmt='%f',delimiter=' ');
np.savetxt(save_path+'/'+name+".txt",img,fmt='%f',delimiter=' ');
fig=plot_image(img,X,Y,bound);
plt.savefig(save_path+"/"+name+".png")
#plt.show();

