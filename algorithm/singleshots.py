###################################
#The Algorithm of Image Processing#
###################################
import numpy as np
import matplotlib.pyplot as plt
import sys
#define the global constant pi=3.1415926....
global PI
PI=np.pi

#Transform the Cartesian coordinates to the polar coordinates
def polar(x,y):
	r=np.sqrt(x**2+y**2);
	PI=np.pi;
	if (x==0 and y>0):
		theta=PI/2;
	elif (x==0 and y<0):
		theta=PI*3/2;
	elif (x<0):
		theta=PI+np.arctan(y/x);
	elif (x>0 and y<0):
		theta=2*PI+np.arctan(y/x);
	elif (x==0 and y==0):
		theta=0;
	else:
		theta=np.arctan(y/x);
	return r,theta

#To calculate the angle difference between two angles in the polar coordinates
def ang_diff(x,y):
	diff=np.abs(x-y);
	if (diff>PI):
		diff=2*PI-diff;
	return diff

#read the data of single-shot images
def read_data(path,dimx,dimy):
	data=np.loadtxt(path);
	img=np.zeros([dimy,dimx]);
	return data,img

def add(data,img,N):
	img_t=np.zeros(np.shape(img));
	r,c=np.shape(data);
	str_num=N;# The particle number in one shell
	M=N*(c-3);# The total particle numbers
	for i in range(3,c):	
		print("image",i-2)
		data1=data[:,i]
		mat=np.reshape(data1,np.shape(img));
		px_old,py_old=np.nonzero(mat);
		p_num=len(px_old)
		for j in range(0,p_num):# Record the new positions with a matrix;
			img[px_old[j]][py_old[j]]=img[px_old[j]][py_old[j]]+1;
	return img/M


def rotate(data,img,N):
	img_t=np.zeros(np.shape(img));
	r,c=np.shape(data);
	str_num=N;# The particle number in one shell
	M=N*(c-3);# The total particle numbers
	for i in range(3,c):
		print("image",i-2)
		data1=data[:,i]
		mat=np.reshape(data1,np.shape(img));
		px_old,py_old=np.nonzero(mat);
		p_num=len(px_old)
		px_center=sum(px_old)/p_num;
		py_center=sum(py_old)/p_num;
		px=px_old#-px_center;# Substract the position of center of mass in the x-direction
		py=py_old#-py_center;# Substract the position of center of mass in the y-direction

		radius=[];
		theta=[];
		theta_diff=[];
		for i in range(0,p_num):
			a,b=polar(px[i],py[i]);# Transform to the polar coordinates
			radius.append(a);
			theta.append(b);
		radius=np.array(radius);
		theta=np.array(theta);

		if N==6:
			str_num=N-1;# Focus on the outermost shell of 6 fermion system
			r_min=min(radius);
			r_i=np.argwhere(radius==r_min);
			r_i=r_i[0];
			theta0=theta[r_i];
			radius=np.delete(radius,r_i);
			theta=np.delete(theta,r_i);

		res=np.sum(theta)/str_num;# Find the rotation angle alpha
		res=res%(2*PI/str_num);# Make sure that the rotation angle alpha is smaller than 2*pi/k in the k-fold axis of symmetric shell.
		rot_theta=theta#-res;

		if N==6:
			radius=np.append(radius,r_min);
			rot_theta=np.append(rot_theta,theta0);

		x=radius*np.cos(rot_theta);# Recover the processed coorindates in the Cartesian coordinates
		y=radius*np.sin(rot_theta);

		# x_t=np.shape(img)[1]/2;
		# y_t=np.shape(img)[0]/2;
		x_t=0;
		y_t=0;
		for i in range(0,p_num):# Record the new positions with a matrix
			xx=int(round(x[i])+x_t);
			yy=int(round(y[i])+y_t);
			if (xx>dimx-1):xx=dimx-1;
			if (yy>dimy-1):yy=dimy-1;
			img[xx][yy]=img[xx][yy]+1;
	return img/M

#Plot figures
def plot_image(img,X,Y,bound):
	fig = plt.figure(figsize=(10,8))
	cax=plt.pcolor(X,Y,img,cmap='rainbow',shading='auto');
	cbar = plt.colorbar(cax, drawedges = False)
	cbar.set_label(r"Configuration Density $\mathcal{S}_{N_s}$",size=25, weight =  'bold')
	cbar.ax.tick_params(labelsize=25 )
	cbar.minorticks_on()
	plt.xlim(-1*bound/2,bound/2);
	plt.ylim(-1*bound/2,bound/2);
	plt.xticks(fontsize=25);
	plt.yticks(fontsize=25);
	plt.xlabel("x",fontsize=30);
	plt.ylabel("y",fontsize=30);
	plt.title("(d) Six bosons",fontsize=30)
	return fig



# path=sys.argv[1];
# save_path=sys.argv[2]
# dim=int(sys.argv[3]);
# i=int(sys.argv[4]);
# N=int(sys.argv[5]);
which='F3';
#path="/home/jiabing/ResearchTraining/MCTDH-X-Tutorial/10.0000000N3M4xSingleShots.dat"
path="/home/jiabing/ResearchTraining/Master_Thesis/data/"+which+"_singleshots.dat";
#path='/home/jiabing/ResearchTraining/MCTDH-X-Tutorial/test/1.00000000N3M5xSingleShots.dat'
#save_path="/lustre/hpe/ws10/ws10.3/ws/xhcmbtb8-thesis/test2";
save_path="/home/jiabing/ResearchTraining/Master_Thesis/data/observable/"
dim=128;
N=3;
dimx=dim;
dimy=dim;
bound=dim/128*8;
X=np.linspace(-1*bound,bound,dimx);
Y=np.linspace(-1*bound,bound,dimy);
# con='N3M4';
####################################
#Plot singleshots
####################################
# print("Processing image "+str(i))
# if i<10:
#         filename=str(i)+'.00000000'+con+'xSingleShots.dat';
# elif (i>=10 and i<100):
#         filename=str(i)+'.0000000'+con+'xSingleShots.dat';
# else:
#         filename=str(i)+'.000000'+con+'xSingleShots.dat';
# path_shots=path+'/'+filename;
# print("LOADING DATA:",i)
# data,img=read_data(path_shots,dimx,dimy);
# img=rotate(data,img,N);
# np.savetxt(save_path+'/singleshots/'+str(i)+".txt",img,fmt='%f',delimiter=' ');
# fig=plot_image(img,X,Y,bound);
# plt.savefig(save_path+'/singleshots/'+str(i)+".png")
# print("Finish image "+str(i))
data,img=read_data(path,dimx,dimy);

img=rotate(data,img,N);
#img=add(data,img,N)
#np.savetxt(save_path+'/'+which+".txt",img,fmt='%f',delimiter=' ');
np.savetxt(save_path+"F1.txt",img,fmt='%f',delimiter=' ');
fig=plot_image(img,X,Y,bound);
#plt.savefig(save_path+"/6NE2.png")
plt.show();
