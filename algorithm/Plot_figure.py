import numpy as np
import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.ticker as ticker
import matplotlib

def fmt(x, pos):
    a, b = '{:.1e}'.format(x).split('e')
    c=round(x*10000,1)
    b = int(b)
    return r'${}$'.format(c)

def plot_image(img,X,Y,bound,R,index):
	ratio=1.2
	fig=plt.figure(figsize=(8,9))
	plt.subplots_adjust(left=0.07,right=0.93, top=0.99,bottom=0.05)
	gs1 = gridspec.GridSpec(3, 2,width_ratios=[ratio,1], height_ratios=[1,1,1]);
	gs1.update(wspace=0, hspace=0);
	ind=["a","b","c","d","e","f"]
	for i in range(6):
		ax=plt.subplot(gs1[i]);
		cax=plt.pcolor(X,Y,img[i],cmap='rainbow',shading='auto');
		if i%2==0:
			pos="left";
			dis_pad=0.2;
			frac_size=0.035/ratio;
		elif i%2==1:
			pos='right';
			dis_pad=0.05;
			frac_size=0.035;
		# fmt=ticker.ScalarFormatter(useMathText=True)
		# fmt.set_powerlimits((0, 0))
		#fmt.set_useLocale(True)
		#ticks_para=[round(i,3) for i in np.linspace(0, np.max(img[i])*1.01, 4)];
		cbar = plt.colorbar(cax,ax = [ax], fraction=frac_size,pad=dis_pad,drawedges = False,location=pos,format=ticker.FuncFormatter(fmt));
		cbar.ax.set_title(r'  $\times 10^{-3}$')
		para_ran=np.linspace(0,np.max(img[i]),4);
		print(para_ran)
		cbar.set_ticks(para_ran)
		# cbar.set_ticks(ticks=ticks_para);
		# cbar.ax.yaxis.set_offset_position("left")
		#lab=r'Configuration density $\mathcal{S}_{N_s}$';
		#lab=r"Density $\rho(x,y)$";
		#cbar.set_label(lab,size=25, weight =  'bold')
		cbar.ax.tick_params(labelsize=15)
		cbar.minorticks_on()
		cbar.update_ticks()
		ran=6;
		ran1=ran+0.5;
		plt.xlim(-1*ran1,ran1);
		plt.ylim(-1*ran1,ran1);
		# ax.set_xticklabels([])
		# ax.set_yticklabels([])
		if i%2==0:
			plt.ylabel("y",fontsize=15);
			plt.yticks(range(-ran,2+ran,2),fontsize=15);
			ax.tick_params(axis='y',direction='in')
		else:
			ax.set_yticklabels([])
		if i>3:
			plt.xlabel("x",fontsize=15);
			plt.xticks(range(-ran,2+ran,2),fontsize=15);
			ax.tick_params(axis='x',direction='in')
		else:
			ax.set_xticklabels([])
		title="("+ind[i]+")"+r"$\mathbf{\mathcal{S}_{N_s}}$(x,y;t="+str(index[i])+")"#+";R="+R[i];
		#title="("+ind[i]+")"+r"$\mathbf{\rho}$(x,y;t="+str(index[i])+")"
		plt.text(-1*ran1*0.92,ran1*0.85,title,fontsize=12,color='white',weight='bold')
		#plt.text(-ran*0.82,ran*0.72,"R="+str(R[i]),fontsize=12,color='white')
		#plt.text(ran*0.75,ran*0.8,r"$\mathcal{S}_{N_s}$",color='white',fontsize=15,weight='bold')
		#plt.xticks(fontsize=25);
		#ax.tick_params(axis='y',direction='in',pad=-42)
		#plt.yticks(fontsize=25);
		# if l[0]==1:
		# 	plt.xlabel("x",fontsize=30);
		# if l[1]==1:
		#plt.ylabel("y",fontsize=30);
		#plt.title(title,fontsize=30)
	#fig.subplots_adjust(wspace=0, hspace=0)
	return 1




def plot_image4(img,X,Y,bound,title):
	ratio=1.15
	fig=plt.figure(figsize=(12,10))
	plt.subplots_adjust(left=0.07,right=0.93, top=0.99,bottom=0.1)
	gs1 = gridspec.GridSpec(2, 2,width_ratios=[ratio,1], height_ratios=[1,1]);
	gs1.update(wspace=0, hspace=0);
	for i in range(4):
		ax=plt.subplot(gs1[i]);
		cax=plt.pcolor(X,Y,img[i],cmap='rainbow',shading='auto');
		if i%2==0:
			pos="left";
			dis_pad=0.15;
			frac_size=0.035/ratio;
		elif i%2==1:
			pos='right';
			dis_pad=0.05;
			frac_size=0.035;
		# fmt=ticker.ScalarFormatter(useMathText=True)
		# fmt.set_powerlimits((0, 0))
		#fmt.set_useLocale(True)
		#ticks_para=[round(i,3) for i in np.linspace(0, np.max(img[i])*1.01, 4)];
		cbar = plt.colorbar(cax,ax = [ax], fraction=frac_size,pad=dis_pad,drawedges = False,
			location=pos,format=ticker.FuncFormatter(fmt));
		cbar.ax.set_title(r'   $\times 10^{-4}$',fontsize=15)
		para_ran=np.linspace(0,np.max(img[i]),4);
		print(para_ran)
		cbar.set_ticks(para_ran)
		# cbar.set_ticks(ticks=ticks_para);
		# cbar.ax.yaxis.set_offset_position("left")
		#lab=r'Configuration density $\mathcal{S}_{N_s}$';
		#lab=r"Density $\rho(x,y)$";
		#cbar.set_label(lab,size=25, weight =  'bold')
		cbar.ax.tick_params(labelsize=20)
		cbar.minorticks_on()
		cbar.update_ticks()
		ran=8;
		ran_step=2;
		ran1=ran;
		plt.xlim(-1*ran1,ran1);
		plt.ylim(-1*ran1,ran1);
		# ax.set_xticklabels([])
		# ax.set_yticklabels([])
		if i%2==0:
			plt.ylabel("y",fontsize=20);
			plt.yticks(range(-ran,ran_step+ran,ran_step),fontsize=20);
			ax.tick_params(axis='y',direction='in')
		else:
			ax.set_yticklabels([])
		if i>1:
			plt.xlabel("x",fontsize=20);
			plt.xticks(range(-ran,ran_step+ran,ran_step),fontsize=20);
			ax.tick_params(axis='x',direction='in')
		else:
			ax.set_xticklabels([])
		plt.text(-1*ran1*0.92,ran1*0.85,title[i],fontsize=20,color='white',weight='bold')
	plt.text(9.9,-6.5,"0.0",fontsize=20,color="black")
	props = dict(boxstyle='square', facecolor='white', alpha=1,edgecolor='none')
	plt.text(-9,-9.2,"(-)8",fontsize=20,color="black",bbox=props)
	plt.text(-26.6,7.85,"(-)8",fontsize=20,color="black",bbox=props)
	return 1


def main(path,save_path,name):
	index=[0,52,79,135];
	R=["21.63","18.09","3.95","2.18"]
	IMAGE=[];
	for i in index:
		path1=path+str(i)+".txt";
		img=np.loadtxt(path1);
		img=img/np.sum(img)*N[i];
		IMAGE.append(img)
	dimx,dimy=np.shape(img)
	bound=dimx/128*8;
	X=np.linspace(-1*bound,bound,dimx);
	Y=np.linspace(-1*bound,bound,dimy);
	fig=plot_image(IMAGE,X,Y,bound,R,index);
	plt.show()
	#plt.savefig(save_path+name+".png",dpi=300)

def main4(path,save_path,name):
	#index=["HO2D/A/1/","HO2D_ani/gamma0.9/1/","HO2D_r4/B0.001/1/"];
	#index=[0,52,79,135]
	index=[195,201,248,302]
	R=["21.63","18.09","3.95","2.18"]
	IMAGE=[];
	#ind=[r"(a)$\mathbf{V_h}$",r"(b)$\mathbf{V_{ani}}$",r"(c)$\mathbf{V_{anh}}$",r"(d)$\mathbf{V_{gau}}$"];
	#ind=[r"(a)$\mathbf{\rho}$",r"(b)$\mathbf{\rho}$",r"(c)$\mathbf{\rho}$",r"(d)$\mathbf{\rho}$"]
	ind=["(a)","(b)","(c)","(d)"]
	name_str=r"$\mathbf{\rho}$"
	#tit_name=["Three fermions","Three bosons","Six fermions","Six bosons"];
	#tit_name=[r"$\mathbf{V_h}$",r"$\mathbf{V_{ani}}$",r"$\mathbf{V_{anh}}$",r"$\mathbf{V_{gau}}$"]
	#path1=["F3","B3","F6","B6"];
	title=[];
	for i in range(4):
		img=np.loadtxt(path+str(index[i])+".txt")
		#img=np.loadtxt(path+path1[i]+".txt")
		# if i<3:
		# 	img=np.loadtxt(path+index[i]+"singleshots/0.txt");
		# else:
		# 	img=np.loadtxt("/home/jiabing/ResearchTraining/Master_Thesis/data/Gau/N6/10/singleshots/0.txt")
		img=img/np.sum(img)*N;
		IMAGE.append(img);
		#title.append(ind[i]+tit_name[i]+": "+name_str+"(x,y)")
		#title.append(ind[i]+tit_name[i]+r": $\mathbf{\mathcal{S}_{N_s}}$(x,y;t=0)")
		#title.append(ind[i]+r"$\mathbf{\mathcal{S}_{N_s}}$(x,y;t="+str(index[i])+");R="+R[i])
		title.append(ind[i]+name_str+"(x,y;t="+str(index[i])+")")
	dimx,dimy=np.shape(img)
	bound=dimx/128*8;
	X=np.linspace(-1*bound,bound,dimx);
	Y=np.linspace(-1*bound,bound,dimy);
	fig=plot_image4(IMAGE,X,Y,bound,title);
	#plt.show()
	plt.savefig(save_path+name+".png",dpi=300)


def plot_image2(img,X,Y,bound,title):
	ratio=1.1
	fig=plt.figure(figsize=(14,6.1))
	plt.subplots_adjust(left=0.07,right=0.93, top=0.99,bottom=0.1)
	gs1 = gridspec.GridSpec(1, 2,width_ratios=[ratio,1], height_ratios=[1]);
	gs1.update(wspace=0, hspace=0);
	for i in range(2):
		ax=plt.subplot(gs1[i]);
		cax=plt.pcolor(X,Y,img[i],cmap='rainbow',shading='auto');
		if i%2==0:
			pos="left";
			dis_pad=0.13;
			frac_size=0.035/ratio;
		elif i%2==1:
			pos='right';
			dis_pad=0.05;
			frac_size=0.035;
		# fmt=ticker.ScalarFormatter(useMathText=True)
		# fmt.set_powerlimits((0, 0))
		#fmt.set_useLocale(True)
		#ticks_para=[round(i,3) for i in np.linspace(0, np.max(img[i])*1.01, 4)];
		cbar = plt.colorbar(cax,ax = [ax], fraction=frac_size,pad=dis_pad,drawedges = False,
			location=pos,format=ticker.FuncFormatter(fmt));
		cbar.ax.set_title(r'   $\times 10^{-3}$',fontsize=15)
		para_ran=np.linspace(0,np.max(img[i]),4);
		print(para_ran)
		cbar.set_ticks(para_ran)
		# cbar.set_ticks(ticks=ticks_para);
		# cbar.ax.yaxis.set_offset_position("left")
		#lab=r'Configuration density $\mathcal{S}_{N_s}$';
		#lab=r"Density $\rho(x,y)$";
		#cbar.set_label(lab,size=25, weight =  'bold')
		cbar.ax.tick_params(labelsize=20)
		cbar.minorticks_on()
		cbar.update_ticks()
		ran=3;
		ran1=ran+0.5;
		plt.xlim(-1*ran1,ran1);
		plt.ylim(-1*ran1,ran1);
		# ax.set_xticklabels([])
		# ax.set_yticklabels([])
		if i%2==0:
			plt.ylabel("y",fontsize=20);
			plt.yticks(range(-ran,1+ran,1),fontsize=20);
			ax.tick_params(axis='y',direction='in')
		else:
			ax.set_yticklabels([])
		plt.xlabel("x",fontsize=20);
		plt.xticks(range(-ran,1+ran,1),fontsize=20);
		ax.tick_params(axis='x',direction='in')
		plt.text(-1*ran1*0.92,ran1*0.85,title[i],fontsize=20,color='white',weight='bold')
	# plt.text(9.9,-6.5,"0.0",fontsize=20,color="black")
	# props = dict(boxstyle='square', facecolor='white', alpha=1,edgecolor='none')
	# plt.text(-8.6,-8.8,"(-)8",fontsize=13,color="black",bbox=props)
	# plt.text(-25.8,7.85,"(-)8",fontsize=13,color="black",bbox=props)
	return 1

def main2(path,save_path,name):
	index=["density","F1"];
	#index=["F2","F3"]
	IMAGE=[];
	title=[r"(a)$\mathbf{\rho}$(x,y)",r"(b)$\mathbf{\mathcal{D}_{N_s}}$(x,y)"]
	#title=[r"(a)$\mathbf{\bar{\mathcal{D}}_{N_s}}$(x,y)",r"(b)$\mathbf{\mathcal{S}_{N_s}}$(x,y)"]
	for i in range(2):
		img=np.loadtxt(path+index[i]+".txt");
		IMAGE.append(img);
	dimx,dimy=np.shape(img)
	bound=dimx/128*8;
	X=np.linspace(-1*bound,bound,dimx);
	Y=np.linspace(-1*bound,bound,dimy);
	fig=plot_image2(IMAGE,X,Y,bound,title);
	#plt.show()
	plt.savefig(save_path+name+".png",dpi=300)

global N
N=3;	
name="figure"+"D"
path="/home/jiabing/ResearchTraining/Master_Thesis/data/scale/density/"
#path="/home/jiabing/ResearchTraining/Master_Thesis/data/observable/"
save_path="/home/jiabing/ResearchTraining/Master_Thesis/Figures/final/";
main4(path,save_path,name)