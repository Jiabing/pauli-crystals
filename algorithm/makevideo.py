import os
import cv2
import sys
N=sys.argv[1]
which=sys.argv[2]
A=sys.argv[3]
filename=sys.argv[4]
# N='3'
# which='density'
# A='1'
# filename='HO2D_r4/B0.001'
if which=='density':
	s1='one-body_density'
elif which=='singleshots':
	s1='configuration_density';

if filename=='HO2D':
	file_dir='/home/jiabing/ResearchTraining/Master_Thesis/data/video/Figures/N'+N+'/'+filename+'/'+A+'/'+which+'/';
	sav_path='/home/jiabing/ResearchTraining/Master_Thesis/data/video/video/N'+N+'/'+filename+'/'+s1+'/';
	save_name=sav_path+filename+'_N'+N+'_A_'+str(int(A)/1000)+'_'+which+'.mp4';
elif filename=='HO2D_r4/B0.001':
	file_dir='/home/jiabing/ResearchTraining/Master_Thesis/data/video/Figures/N'+N+'/'+filename+'/'+A+'/'+which+'/';
	sav_path='/home/jiabing/ResearchTraining/Master_Thesis/data/video/video/N'+N+'/'+filename+'/'+s1+'/';
	save_name=sav_path+'HO2D_r4_N'+N+'_A_'+str(int(A)/100)+'_B0.001_'+which+'.mp4';
elif filename=='HO2D_r4/A0.1':
	file_dir='/home/jiabing/ResearchTraining/Master_Thesis/data/video/Figures/N'+N+'/'+filename+'/'+A+'/'+which+'/';
	sav_path='/home/jiabing/ResearchTraining/Master_Thesis/data/video/video/N'+N+'/'+filename+'/'+s1+'/';
	save_name=sav_path+'HO2D_r4_N'+N+'_A_0.1'+'_B'+str(int(A)/1000)+'_'+which+'.mp4';	
elif filename=='Gau':
	file_dir='/home/jiabing/ResearchTraining/Master_Thesis/data/video/Figures/N'+N+'/'+filename+'/'+A+'/'+which+'/';
	sav_path='/home/jiabing/ResearchTraining/Master_Thesis/data/video/video/N'+N+'/'+filename+'/'+s1+'/';
	save_name=sav_path+filename+'_N'+N+'_A_'+str(int(A)/100)+'_'+which+'.mp4';
elif filename=='HO2D_ani/gamma0.9':
	file_dir='/home/jiabing/ResearchTraining/Master_Thesis/data/video/Figures/N'+N+'/'+filename+'/'+A+'/'+which+'/';
	sav_path='/home/jiabing/ResearchTraining/Master_Thesis/data/video/video/N'+N+'/'+filename+'/'+s1+'/';
	save_name=sav_path+'HO2D_ani_N'+N+'_A_'+str(int(A)/100)+'_gamma0.9_'+which+'.mp4';
elif filename=='HO2D_ani/A0.01':
	file_dir='/home/jiabing/ResearchTraining/Master_Thesis/data/video/Figures/N'+N+'/'+filename+'/'+A+'/'+which+'/';
	sav_path='/home/jiabing/ResearchTraining/Master_Thesis/data/video/video/N'+N+'/'+filename+'/'+s1+'/';
	save_name=sav_path+'HO2D_ani_N'+N+'_A_0.01'+'_gamma0.9'+A+'_'+which+'.mp4';	
else:
	print("Error:Your input parameters are wrong!!!")


list=[]
for root,dirs,files in os.walk(file_dir):
    for file in files:
       list.append(file)

image=cv2.imread(file_dir+list[0])
image_info=image.shape
height=image_info[0]
width=image_info[1]
size=(height,width)
fps=5
fourcc=cv2.VideoWriter_fourcc(*"mp4v")
video = cv2.VideoWriter(save_name, cv2.VideoWriter_fourcc(*"mp4v"), fps, (width,height))


for i in range(0,len(list)):
	img=cv2.imread(file_dir+str(i)+'.png');
	video.write(img);
video.release()
print(filename,N,A,which)