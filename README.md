# Pauli crystals

Repository for the data and algorithm used in the master's thesis titled "Pauli crystals: Detection, Correlations and Dynamics".

# Abstract

Pauli crystals are a particular type of crystalline phase: a geometric arrangement of non-interacting identical fermions in a trap. The Pauli exclusion principle that two identical fermions are forbidden to occupy the same one-particle state results in a configuration correlation between fermions. Pauli crystals were observed in experiments recently by trapping a few ultracold atoms in a quasi-two-dimensional harmonic potential. The experiment further reproduced the deformation of geometric structure of Pauli crystals by modulating the trap frequency. To model the dynamics in these experiments, we employ the multiconfiguration time-dependent Hartree method for indistinguishable particles (MCTDH-X). In our simulations, the geometric correlation structures of Pauli crystals with N = 3 and N = 6 fermions are revealed by a collection of processed single-shot images. To investigate the reason for the melting, we study the influence of a periodic driving and the experimental imperfections in the trap (anisotropy and anharmonicity) on the system dynamics. Our simulation results in different trap models show that the melting of Pauli crystals is absent in traps without imperfections. We demonstrate that the melting is only triggered by a sufficiently large shaking amplitude in traps with imperfections.

## Algorithm
- [ ] [singleshots.py](https://gitlab.com/Jiabing/pauli-crystals/-/blob/main/algorithm/singleshots.py)
It has been used to process the singleshot images produced by MCTDHX_analysis.and plot the configuration density. 
- [ ] [density.py](https://gitlab.com/Jiabing/pauli-crystals/-/blob/main/algorithm/density.py)
It has been used to plot the one-body density from the original density data produced by MCTDHX_analysis.
- [ ] [Plot_figure.py](https://gitlab.com/Jiabing/pauli-crystals/-/blob/main/algorithm/Plot_figure.py)
It has been used to plot all one-body density and configuration density in the thesis. Three plot functions are defined in terms of the number of subfigures. For instance, [plot_image6] is to plot 6 subfigures in a figure.
- [ ] [Recognition_function](https://gitlab.com/Jiabing/pauli-crystals/-/blob/main/algorithm/Recognition_function.py)
It has been used to plot the variance of the one-body density and configuration density, Figs. 3.1.3 and 3.1.4.
- [ ] [Get_R_Theta.py](https://gitlab.com/Jiabing/pauli-crystals/-/blob/main/algorithm/Get_R_Theta.py)
It has been used to get the variance of the one-body density or configuration density. 
- [ ] [modulation.py](https://gitlab.com/Jiabing/pauli-crystals/-/blob/main/algorithm/modulation.py)
It has been used to plot the modulation process Fig. 2.2.1 in the thesis.
- [ ] [makevideo.py](https://gitlab.com/Jiabing/pauli-crystals/-/blob/main/algorithm/makevideo.py)
It has been used to collect the images into a video.

## Parameter_setting
It includes the **MCTDHX.inp** and **analysis.inp** used in all relaxation and propagation computations.<br>
**Relaxation mode** is to compute the ground state of the system and prepare the initial state for the next propagation computation.<br>
**Propagation mode** is to compute the dynamical process.<br>
We defined four potentials investigated in our work in the [Get_1bodyPotential.F](https://gitlab.com/Jiabing/pauli-crystals/-/blob/main/Parameter_setting/Get_1bodyPotential.F).

## Videos
It contains the videos of the one-body and configuration densities for the all sets of parameters.<br>
**HO2D**: Isotropic harmonic trap.(a) Shaking amplitude A: 0.001-0.01.<br>
**HO2D_r4**: Isotropic anharmonic trap. (a) Shaking amplitude A:0.01-0.1 with a fixed anharmonicity B=0.001. (b) Anharmonicity B:0.001-0.01 with a fixed shaking amplitude A=0.1.<br>
**HO2D_ani**: Anisotropic harmonic trap. (a) Shaking amplitude A:0.01-0.1 with a fixed anistropy gamma=0.9. (b) Anisotropy gamma:0.91-0.99 with a fixe shaking amplitude A=0.01.<br>
**Gau**: Gaussian trap. (a) Shaking amplitude A:0.01-0.1.